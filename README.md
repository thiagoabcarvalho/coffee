Este projeto faz parte da certificação *front-end do Zero ao Pró* da escola [mentorama](https://mentorama.com.br).

## Front-end iniciante

3 meses de duração

## HTML

- Semântica
- Marcação
- Estrutura
- Acessibilidade
- SEO

## CSS

- estilos e formatação
- Seletores, Classes
- Posicionamento
- Display
- FlexBox e Grid
- Normalize

## Introdução javascript

- variáveis
- DOM

## Git

- Conceitos e arquitetura
- Comandos (básicos)
- Alteração de arquivos
- Repositório remoto com o GitHub

A branch ( *pro* )deste arquivo faz parte das atividades do curso Pró

*esse conteúdo faze parte da trila Front-end do 0 ao Pró: que contém 4 cursos de 3 meses cada*

![layout da página](./img/Landing%20Page%20%E2%84%96%202%20-%20Coffee.png)

